import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';


const baseUrl = 'http://localhost:8080/api/tutorials'

@Injectable({
  providedIn: 'root'
})
export class TutorialService {

  constructor(private http: HttpClient) { }

  getAll():Observable<any> {
    return this.http.get(baseUrl);
  }

  get(id):Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data):Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('image',data.image);
    formData.append('title',data.title);
    formData.append('description',data.description);

    const req = new HttpRequest('POST',baseUrl,formData,{reportProgress: true,
      responseType: 'json'})

    return this.http.request(req);
  }

  update(id, data):Observable<any> {
    
    const formData: FormData = new FormData();
    formData.append('image',data.image);
    formData.append('title',data.title);
    formData.append('description',data.description);
    formData.append('published',data.published);
    

    return this.http.put(`${baseUrl}/${id}`,formData);
  }

  delete(id):Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll():Observable<any> {
    return this.http.delete(baseUrl);
  }

  findByTitle(title):Observable<any> {
    return this.http.get(`${baseUrl}?title=${title}`);
  }


  createComment(data):Observable<any>{
    return this.http.post(baseUrl+'/comment', data);
  }

  deleteComment(id):Observable<any>{
    return this.http.delete(baseUrl+'/comment/'+id);
  }

  getpublished():Observable<any>{
    return this.http.get(baseUrl+'/published');
  }
}
