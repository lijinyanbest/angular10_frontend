import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { HomeComponent } from './component/home/home.component';
import { ProfileComponent } from './component/profile/profile.component';
import { BoardAdminComponent } from './component/board-admin/board-admin.component';
import { BoardModeratorComponent } from './component/board-moderator/board-moderator.component';
import { BoardUserComponent } from './component/board-user/board-user.component';
import { AddTutorialComponent } from './component/add-tutorial/add-tutorial.component';
import { TutorialDetailsComponent } from './component/tutorial-details/tutorial-details.component';
import { TutorialListComponent } from './component/tutorial-list/tutorial-list.component';
import { UserTutorialdetailComponent } from './component/user-tutorialdetail/user-tutorialdetail.component';
import { ChatComponent } from './component/chat/chat.component';

const routes: Routes = [
  {path:'tutorials', component:TutorialListComponent},
  {path:'tutorials/:id', component:TutorialDetailsComponent},
  {path:'add', component:AddTutorialComponent},
  {path:'usertutorial/:id', component:UserTutorialdetailComponent},
  {path:'chat', component:ChatComponent},
  {path:'home', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'profile', component:ProfileComponent},
  {path:'user', component:BoardUserComponent},
  {path:'mod', component:BoardModeratorComponent},
  {path:'admin', component:BoardAdminComponent},
  {path:'', redirectTo : 'home', pathMatch: 'full'}
];

@NgModule({
  declarations: [],
  imports: [
     RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
