import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/_services/tutorial.service';
import { ActivatedRoute, Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-tutorial-details',
  templateUrl: './tutorial-details.component.html',
  styleUrls: ['./tutorial-details.component.css']
})
export class TutorialDetailsComponent implements OnInit {

  currentTutorial = null;
  message = "";
  messagecom = "";

  selectedFiles: FileList;
  typemessage="";

  currentComment = "";
  tutomessage="";

  comment = {
    name:"",
    text:"",
    tutorialid:-1,
  };

  constructor(
    private tutorialService: TutorialService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.message="";
    this.getTutorial(this.route.snapshot.paramMap.get('id'));
  }

  getTutorial(id):void{
    this.tutorialService.get(id).subscribe(
      data => {
        this.currentTutorial = data;
        this.currentComment = data.comment;
      }, error => {
        console.log(error);
      });

      
  }

  updatePublished(status):void{
    
    const data = {
      title: this.currentTutorial.title,
      description:this.currentTutorial.description,
      published: status
    };

    this.tutorialService.update(this.currentTutorial.id, data)
      .subscribe(response => {
        this.currentTutorial.published = status;
        this.tutomessage = `The tutorial was ${status ? "Published" : "Pending"} successfully!`;
      }, error => {
        console.log(error);
      });
  }

  updateTutorial():void {

    const data = {
      title: this.currentTutorial.title,
      description:this.currentTutorial.description,
      published: this.currentTutorial.published,
      image:this.selectedFiles
    };

    this.tutorialService.update(this.currentTutorial.id, data)
      .subscribe(response => {
        this.tutomessage = "The tutorial was updated successfully!";
      }, error => {
        console.log(error);
      });
  }

  deleteTutorial():void{
    this.tutorialService.delete(this.currentTutorial.id)
      .subscribe(response => {
        this.router.navigate(['/tutorials']);
      },error => {
        console.log(error);
      });
  }

  createComment():void{

    const data = {
        name:this.comment.name,
        text:this.comment.text,
        tutorialid:this.currentTutorial.id
    };

    console.log("tutorial detail:name " + data.name);
    console.log("tutorial detail: tutorialid" + data.tutorialid);

    this.tutorialService.createComment(data)
      .subscribe(response => {
        this.getTutorial(response.tutorialId);
        this.comment = {
          name:"",
          text:"",
          tutorialid:-1,
        };
        this.messagecom = "comment added!";
      }, error => {
        console.log(error);
      });
  }

  deleteComment(id):void{
    console.log("delete comment : " + id);
      this.tutorialService.deleteComment(id).subscribe(
        response =>{
          this.getTutorial(this.currentTutorial.id);
          console.log(response);
          this.message=response.message;
        },error => {
          console.log(error);
        });
  }

  selectFile(event): void{
    const files = event.target.files;
    const file = files.item(0);
    let isImage = true;
    if (!(file.type.match("image.*"))){
      isImage = false;
      this.typemessage = "invalid format!";
    }

    if(isImage){
      this.selectedFiles = file;
      this.typemessage="";  
    }else{
      this.selectedFiles = undefined;

    }
  }
}
