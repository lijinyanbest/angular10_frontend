import { Component } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css']
})
export class BoardModeratorComponent{

  page = 1;
  pageSize = 4;
  collectionSize :any;
  users: any;

  constructor(private userService:UserService) {
    this.getallUser();
   }

  getallUser():void{
    this.userService.getModeratorBoard().subscribe(data => {
      this.collectionSize=data.length;
      this.users = data
      .map((user, i) => ({id: i + 1, ...user}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    },
    err => {
      this.users = JSON.parse(err.error).message;
    });
  }

}
