import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/_services/tutorial.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-tutorialdetail',
  templateUrl: './user-tutorialdetail.component.html',
  styleUrls: ['./user-tutorialdetail.component.css']
})
export class UserTutorialdetailComponent implements OnInit {

  currentTutorial = null;
  message = "";

  currentComment = "";

  comment = {
    name:"",
    text:"",
    tutorialid:-1,
  };

  constructor(
    private tutorialService: TutorialService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.message="";
    this.getTutorial(this.route.snapshot.paramMap.get('id'));
  }

  getTutorial(id):void{
    console.log("user tutorial detial: " + id);
    this.tutorialService.get(id).subscribe(
      data => {
        this.currentTutorial = data;
        this.currentComment = data.comment;
      }, error => {
        console.log(error);
      });
  }

  createComment():void{

    const data = {
        name:this.comment.name,
        text:this.comment.text,
        tutorialid:this.currentTutorial.id
    };
    this.tutorialService.createComment(data)
      .subscribe(response => {
        this.getTutorial(response.tutorialId);
        this.message="Comment has been added sucessfully!";
        this.comment = {
          name:"",
          text:"",
          tutorialid:-1,
        };
      }, error => {
        console.log(error);
      });
  }

}
