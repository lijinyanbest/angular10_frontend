import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTutorialdetailComponent } from './user-tutorialdetail.component';

describe('UserTutorialdetailComponent', () => {
  let component: UserTutorialdetailComponent;
  let fixture: ComponentFixture<UserTutorialdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTutorialdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTutorialdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
