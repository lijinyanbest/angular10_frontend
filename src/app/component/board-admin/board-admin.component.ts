import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {

  users:any;

  constructor(private userService:UserService) { }

  ngOnInit(): void {

    this.userService.getAdminBoard().subscribe(data => {
      this.users = data;
    },
    err => {
      this.users = JSON.parse(err.error).message;
    });
  }

}
