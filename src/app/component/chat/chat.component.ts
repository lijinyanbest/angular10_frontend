import { Component, OnInit } from '@angular/core';

import * as io from 'socket.io-client';

const SOCKET_ENDPOINT = 'localhost:8080';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  socket;
  message:string;
  username:string;
  connected = false;
  constructor() {
  }

  ngOnInit() {
  }


  setupSocketConnection(){
    if(this.username){
      this.connected = true;
      this.socket = io(SOCKET_ENDPOINT);
      this.appendMessage("You joined! " + this.username);

      this.socket.emit('new-user', this.username);
      this.socket.on('new-broadcast', (data) => {
        if(data){
          const element = document.createElement('li');
          element.innerHTML = `${data.name}:${data.message}`;
          element.style.background = 'white';
          element.style.padding = '15px 30px';
          element.style.margin = '10px';
          document.getElementById('message-list').appendChild(element);
        }
    });

    this.socket.on('user-connected', name => {
      this.appendMessage(`${name} connected`);
    });

    this.socket.on('user-disconnected', name => {
      this.appendMessage(`${name} dicconnected`);
    });

    }else{
      console.log("please enter a user name!");
    }


    
  }



  SendMessage(){
    this.socket.emit('new-message',this.message);
    const element = document.createElement('li');
    element.innerHTML = "YOU:" + this.message;
    element.style.background = 'white';
    element.style.padding = '15px 30px';
    element.style.margin = '10px';
    element.style.textAlign ='right';
    document.getElementById('message-list').appendChild(element);
    this.message=""
  }

  appendMessage(message){
    const element = document.createElement('li');
    element.innerHTML = message;
    element.style.padding = '15px 30px';
    element.style.margin = '10px';
    element.style.textAlign ='right';
    document.getElementById('message-list').appendChild(element);
  }
}
