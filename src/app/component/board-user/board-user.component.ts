import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/_services/tutorial.service';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit {

  tutorials:any;
  currentTutorial = null;
  currentIndex = -1;
  title = "";

  constructor(private tutorialService: TutorialService) { }

  ngOnInit(): void {
    this.retrieveTutorials();
  }

  
  retrieveTutorials():void{

    this.tutorialService.getpublished().subscribe(
        data => {
          this.tutorials = data;
          console.log("tutorial list ts: " + data);
        }, error => {
          console.log(error);
        });
  }

  refreshList():void{
    this.retrieveTutorials();
    this.currentTutorial = null;
    this.currentIndex = -1;
  }

  setActiveTutorial(tutorial, index):void{
    this.currentTutorial = tutorial;
    this.currentIndex = index;
  }

  searchTitle(): void {
    this.tutorialService.findByTitle(this.title)
      .subscribe(data => {
        this.tutorials = data;
      }, error => {
        console.log(error);
      });
  }

}
