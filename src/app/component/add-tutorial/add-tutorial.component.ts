import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/_services/tutorial.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-tutorial',
  templateUrl: './add-tutorial.component.html',
  styleUrls: ['./add-tutorial.component.css']
})
export class AddTutorialComponent implements OnInit {
  
  selectedFiles: FileList;
  progress = 0 ;

  tutorial = {
    title:'',
    description:'',
    published:false,
    image: File
  };

  typemessage="";

  submitted = false;

  
  constructor(private tutorialService: TutorialService) { }

  ngOnInit(): void {
  }

  selectFile(event): void{
    const files = event.target.files;
    const file = files.item(0);
    let isImage = true;
    if (!(file.type.match("image.*"))){
      isImage = false;
      this.typemessage = "invalid format!";
    }

    if(isImage){
      this.selectedFiles = file;
      this.typemessage="";  
    }else{
      this.selectedFiles = undefined;

    }
  }


  saveTutorial():void{
    this.progress = 0;

    const data = {
      title:this.tutorial.title,
      description:this.tutorial.description,
      image:this.selectedFiles
    };
    this.tutorialService.create(data).subscribe(response => {
      this.submitted = true;
      if(response.type === HttpEventType.UploadProgress){
        this.progress = Math.round(100 * response.loaded / response.total);
      }else if (response instanceof HttpResponse){
        console.log("upload file,");
      }
    }, error => {
      this.progress = 0;
      console.log("add tutorial err: " + error.message);
    });
  }

  newTutorial():void{
    this.progress = 0;
    this.submitted = false;
    this.tutorial = {
      title:'',
      description:'',
      published:false,
      image: File
    };
  }

}
